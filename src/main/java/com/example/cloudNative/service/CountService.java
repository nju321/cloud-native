package com.example.cloudNative.service;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

/**
 * @author Liululin
 * @date 2023/8/5 - 10:21
 */

@Service
public class CountService {

    private final Counter requestCounter;

    public CountService(MeterRegistry meterRegistry) {
        this.requestCounter = Counter.builder("api_requests_total")
                .description("Total number of API requests")
                .register(meterRegistry);
    }

    public double visit() {
        requestCounter.increment();
        return requestCounter.count();
    }
}


