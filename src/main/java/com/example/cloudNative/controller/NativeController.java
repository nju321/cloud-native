package com.example.cloudNative.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liululin
 * @date 2023/8/5 - 10:11
 */


@ResponseBody
@RestController
public class NativeController {
    @GetMapping("/native")
    public String hello(){
        return "native";
    }
}
