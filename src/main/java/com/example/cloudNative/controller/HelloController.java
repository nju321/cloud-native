package com.example.cloudNative.controller;

import com.example.cloudNative.limit.Limit;
import com.example.cloudNative.service.CountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * @author Liululin
 * @date 2023/8/5 - 10:10
 */
@Slf4j
@RestController
@ResponseBody

public class HelloController {
    @Autowired
    CountService countService;
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    @GetMapping("/hello")
    @Limit(key = "hello", permitsPerSecond = 1, timeout = 500, timeunit = TimeUnit.MILLISECONDS,msg = "当前排队人数较多，请稍后再试！")
    public String hello(){
        log.info(LocalDateTime.now().format(dtf) +"  请求成功！" );
        log.info("第" +countService.visit() +"次请求成功");
        return "hello";
    }
}
