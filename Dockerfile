FROM openjdk:17

MAINTAINER nju32

VOLUME /tmp

ADD cloud-native-0.0.1-SNAPSHOT.jar cloud-native.jar

RUN bash -c 'touch /cloud-native.jar'
ENTRYPOINT ["java", "-jar", "/cloud-native.jar"]

EXPOSE 9001
